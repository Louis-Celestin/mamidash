import React from 'react'
import style from "./Loader.module.css"

export default function Loader() {
  return (
    <div style={{ display: "flex", justifyContent: "center", alignItems: "center", height: "100%" }} >
      <div className={style.Loader}>

      </div>
    </div>
  )
}
