import React, { Suspense } from "react";

// ** Router Import
import Router from "./router/Router";
import Login from "./views/Login";

const App = () => {

  return (
    <Suspense fallback={null}>
      <Router />
    </Suspense>
  )
};

export default App;
