// ** React Imports
import { useState } from 'react'

// ** Third Party Components
import Flatpickr from 'react-flatpickr'
import { User, Briefcase, Mail, Calendar, DollarSign, X } from 'react-feather'

// ** Reactstrap Imports
import { Modal, Input, Label, Button, ModalHeader, ModalBody, InputGroup, InputGroupText } from 'reactstrap'
import Toggle from 'react-toggle'

// ** Styles
import '@styles/react/libs/flatpickr/flatpickr.scss'
import './module.modal.css'
import Loader from '../../../../components/Loader/Loader'
import Swal from 'sweetalert2'

const AddNewModal = ({ open, handleModal }) => {
    // ** State
    // const [Picker, setPicker] = useState(new Date())

    // ** Custom close btn
    const CloseBtn = <X className='cursor-pointer' size={15} onClick={handleModal} />
    const [nom, setNom] = useState()
    const [description, setDescription] = useState()
    const [image, setImage] = useState()
    const [visibilite, setVisibilite] = useState()
    const [position, setPosition] = useState()

    const [isload, setIsLoad] = useState(false)

    const handleImageChange = (event) => {
        setImage(event.target.files[0])
    }
    const handleReset = () => {
        setImage()
    }

    console.log(image)

    const handleToggle = (e) => {
        setVisibilite(!visibilite)
    }


    const handleSubmit = async () => {
        console.table(visibilite, nom, description, image, position)
        const regex = /^\d+$/;
        if (!nom || !description || !image || !position) {
            Swal.fire({
                title: "Tous les champ sont obligatoures",
                icon: 'error'
            })
        } else if (!regex.test(position)) {
            Swal.fire({
                title: "La position doit etre en entier",
                icon: 'error'
            })
        } else {
            setIsLoad(true)
            const token = JSON.parse(localStorage.getItem("connectedId"))
            const formData = new FormData()
            formData.append('file', image)
            await fetch('https://api.mamifresh.ci/api/admin/category/create', {
                method: "POST",
                headers: {
                    Accept: "application/json",
                    "Content-Type": "application/json",
                    Authorization: `Bearer ${token}`
                },
                body: JSON.stringify({
                    image: formData,
                    name: nom,
                    description
                })
            }).then((res) => {
                if (res.status == 'success') {
                    setIsLoad(false)
                    Swal.fire({
                        title: "Enregistrement effectué",
                        icon: "success"
                    })
                    setTimeout(() => {
                        window.location.replace("/products/categories/list")
                    }, 3000)
                }
            }).catch((err) => {
                setIsLoad(false)
                Swal.fire({
                    title: "Enregistrement non effectué",
                    icon: 'error'
                })

                console.log(err)
            })
        }
    }

    return (
        <Modal
            isOpen={open}
            toggle={handleModal}
            className='sidebar-sm'
            modalClassName='modal-slide-in'
            contentClassName='pt-0'
        >
            <ModalHeader className='mb-1' toggle={handleModal} close={CloseBtn} tag='div'>
                <h5 className='modal-title'>Nouvelle Catégorie</h5>
            </ModalHeader>
            <ModalBody className='flex-grow-1'>
                <div className='mb-1'>
                    <Label className='form-label' for='full-name'>
                        Nom de la Catégorie
                    </Label>
                    <InputGroup>
                        <Input id='full-name' placeholder='Plat de resistance' onChange={(e) => setNom(e.target.value)} />
                    </InputGroup>
                </div>
                <div className='mb-1'>
                    <Label className='form-label' for='post'>
                        Description
                    </Label>
                    <InputGroup>
                        <Input id='post' type='textarea' placeholder='Une description...' onChange={(e) => setDescription(e.target.value)} />
                    </InputGroup>
                </div>
                <div className='mb-1'>
                    <div className='mb-1'>
                        {!image ? <Input type='file' id='image' onChange={handleImageChange} /> : <img src={image} alt="my image" />}
                        <Button className='me-1' color='primary' onClick={handleReset}>
                            Effacer
                        </Button>
                    </div>
                </div>
                <div className='mb-1'>
                    <Label className='form-label' for='email'>
                        Visibilité
                    </Label>
                    <InputGroup>
                        <Toggle
                            id='cheese-status'
                            onChange={handleToggle}
                            defaultChecked={false}

                        />
                        <label htmlFor='cheese-status'></label>
                    </InputGroup>
                </div>
                <div className='mb-1'>
                    <Label className='form-label' for='salary'>
                        Position
                    </Label>
                    <InputGroup>
                        <Input type='number' id='salary' onChange={(e) => setPosition(e.target.value)} />
                    </InputGroup>
                </div>
                <Button className='me-1' color='primary' onClick={handleSubmit}>
                    {isload ? <Loader /> : "Créer"}
                </Button>
            </ModalBody>
        </Modal>
    )
}
export default AddNewModal