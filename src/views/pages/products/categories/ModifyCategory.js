// ** React Imports
import { useState } from 'react'

// ** Third Party Components
import Flatpickr from 'react-flatpickr'
import { User, Briefcase, Mail, Calendar, DollarSign, X } from 'react-feather'

// ** Reactstrap Imports
import { Modal, Input, Label, Button, ModalHeader, ModalBody, InputGroup, InputGroupText } from 'reactstrap'
import Toggle from 'react-toggle'

// ** Styles
import '@styles/react/libs/flatpickr/flatpickr.scss'
import './module.modal.css'

const AddNewModal = ({ open, handleModal }) => {
    // ** State
    // const [Picker, setPicker] = useState(new Date())

    // ** Custom close btn
    const CloseBtn = <X className='cursor-pointer' size={15} onClick={handleModal} />
    return (
        <Modal
            isOpen={open}
            toggle={handleModal}
            className='sidebar-sm'
            modalClassName='modal-slide-in'
            contentClassName='pt-0'
        >
            <ModalHeader className='mb-1' toggle={handleModal} close={CloseBtn} tag='div'>
                <h5 className='modal-title'>Nouvelle Catégorie</h5>
            </ModalHeader>
            <ModalBody className='flex-grow-1'>
                <div className='mb-1'>
                    <Label className='form-label' for='full-name'>
                        Nom de la Catégorie
                    </Label>
                    <InputGroup>
                        <InputGroupText>
                            <User size={15} />
                        </InputGroupText>
                        <Input id='full-name' placeholder='Bruce Wayne' />
                    </InputGroup>
                </div>
                <div className='mb-1'>
                    <Label className='form-label' for='post'>
                        Description
                    </Label>
                    <InputGroup>
                        <InputGroupText>
                            <Briefcase size={15} />
                        </InputGroupText>
                        <Input id='post' placeholder='Web Developer' />
                    </InputGroup>
                </div>
                <div className='mb-1'>
                    <Label className='form-label' for='email'>
                        Image
                    </Label>
                    <InputGroup>
                        <InputGroupText>
                            <Mail size={15} />
                        </InputGroupText>
                        <Input type='email' id='email' placeholder='brucewayne@email.com' />
                    </InputGroup>
                </div>
                <div className='mb-1'>
                    <Toggle
                        id='cheese-status'
                    />
                    <label htmlFor='cheese-status'></label>
                </div>
                <div className='mb-1'>
                    <Label className='form-label' for='salary'>
                        Position
                    </Label>
                    <InputGroup>
                        <InputGroupText>
                            <DollarSign size={15} />
                        </InputGroupText>
                        <Input type='number' id='salary' />
                    </InputGroup>
                </div>
                <Button className='me-1' color='primary' onClick={handleModal}>
                    Créer
                </Button>
                <Button color='secondary' onClick={handleModal} outline>
                    Annuler
                </Button>
            </ModalBody>
        </Modal>
    )
}
export default AddNewModal