// ** React Imports
import { ChangeEvent, useEffect, useState } from 'react'

// ** Third Party Components
import Flatpickr from 'react-flatpickr'
import { X } from 'react-feather'

// ** Reactstrap Imports
import { Modal, Input, Label, Button, ModalHeader, ModalBody, InputGroup, InputGroupText } from 'reactstrap'

// ** Styles
import '@styles/react/libs/flatpickr/flatpickr.scss'

import Swal from 'sweetalert2'

import { Icons } from 'react-toastify'

const AddNewModal = ({ open, handleModal, datas }) => {
    const token = JSON.parse(localStorage.getItem("connectedId"))
    const [options, setOptions] = useState([])

    const [nom, setNom] = useState()
    const [description, setDescription] = useState()
    const [prix, setPrix] = useState()
    const [image, setImage] = useState()
    const [categorie, setCategorie] = useState()
    const [filter, setFilter] = useState()
    const [qte, setQte] = useState()

    const handleFileChange = event => {
        const fileObj = event.target.files && event.target.files[0];
        if (!fileObj) {
            return;
        }

        console.log('fileObj is', fileObj);

        // 👇️ reset file input
        event.target.value = null;

        // 👇️ is now empty
        console.log(event.target.files);

        // 👇️ can still access file object here
        console.log(fileObj);
        console.log(fileObj.name);
        setImage(fileObj.name)
        console.log(URL.createObjectURL(fileObj))
    };

    console.log(nom)

    const handleReset = () => {
        setImage(null)
    }
    const hanldeSubmit = async () => {

        if (!nom || !description || !prix || !image || !categorie || !filter || !qte) {
            Swal.fire({
                title: "Tous les champs sont obligatoires",
                icon: 'error'
            })
        } else {
            await fetch('https://api.mamifresh.ci/api/admin/product/create',
                {
                    headers: {
                        Accept: "application/json",
                        "Content-Type": "application/json",
                        Authorization: `Bearer ${token}`
                    },
                    method: "POSt",
                    body: JSON.stringify({
                        name: nom,
                        description,
                        price: prix,
                        quantity: qte,
                        category_id: categorie,
                        images: image,
                        filtre: filter
                    })
                }).then((result) => {
                    if (result.data) {
                        console.log(result)
                        Swal.fire({
                            title: "Enregistrement effectué"
                        })
                        setTimeout(() => {
                            window.location.replace("/products/list")
                        }, 2500)

                    }
                }).catch((err) => {
                    console.log("Erreur d'enregistrement", err)
                    Swal.fire({
                        title: "Enregistrement non effectué"
                    })
                    setTimeout(() => {
                        window.location.replace("/products/list")
                    }, 2500)

                })
        }

    }




    // ** State
    const [Picker, setPicker] = useState(new Date())
    // ** Custom close btn
    const CloseBtn = <X className='cursor-pointer' size={15} onClick={handleModal} />
    useEffect(() => {
        setOptions(datas)
    }, [])
    return (
        <Modal
            isOpen={open}
            toggle={handleModal}
            className='sidebar-sm'
            modalClassName='modal-slide-in'
            contentClassName='pt-0'
        >
            <ModalHeader className='mb-1' toggle={handleModal} close={CloseBtn} tag='div'>
                <h5 className='modal-title'>Nouveau Produit</h5>
            </ModalHeader>
            <ModalBody className='flex-grow-1'>
                <div className='mb-1'>
                    <Label className='form-label' for='name'>
                        Nom du produit
                    </Label>
                    <InputGroup>
                        <Input id='full-name' onChange={(e) => setNom(e.target.value)} placeholder='Placali' />
                    </InputGroup>
                </div>
                <div className='mb-1'>
                    <Label className='form-label' for='post' >
                        description
                    </Label>
                    <InputGroup>

                        <Input id='post' onChange={(e) => setDescription(e.target.value)} placeholder='un plat tres prisé des baoulé' type='textarea' />
                    </InputGroup>
                </div>
                <div className='mb-1'>
                    {!image ? <input type="file" onChange={handleFileChange} /> : image}
                    <Button className='me-1' color='primary' onClick={handleReset}>
                        Effacer
                    </Button>
                </div>
                <div className='mb-1'>
                    <Label className='form-label' for='email'>
                        Prix
                    </Label>
                    <InputGroup>

                        <Input type='number' onChange={(e) => setPrix(e.target.value)} id='prix' placeholder='12000' />
                    </InputGroup>
                </div>
                <div className='mb-1'>
                    <Label className='form-label' for='jqte'>
                        Quantité
                    </Label>
                    <InputGroup>

                        <Input type='number' id='qte' onChange={(e) => setQte(e.target.value)} placeholder='12' />
                    </InputGroup>
                </div>
                <div className='mb-1'>
                    <Label className='form-label' for='categories'>
                        Catégorie
                    </Label>
                    <InputGroup>

                        <Input
                            id="categories"
                            name="select"
                            type="select"
                            onChange={(e) => setCategorie(e.target.value)}
                        >
                            {datas.map((data) => (
                                <option key={data._id} value={data._id}>{data.name}</option>
                            ))}
                        </Input>
                    </InputGroup>
                </div>
                <div className='mb-1'>
                    <Label className='form-label' for='salary'>
                        Filtre
                    </Label>
                    <InputGroup>
                        <Input type='text' id='filtre' onChange={(e) => setFilter(e.target.value)} />
                    </InputGroup>
                </div>
                <Button className='me-1' color='primary' onClick={hanldeSubmit} >
                    Créer
                </Button>
                <Button color='secondary' onClick={handleModal} outline>
                    Annuler
                </Button>
            </ModalBody>
        </Modal >
    )
}
export default AddNewModal
