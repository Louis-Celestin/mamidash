// ** Custom Components
import Avatar from '@components/avatar'

// ** Third Party Components
import axios from 'axios'
import { MoreVertical, Edit, FileText, Archive, Trash } from 'react-feather'

// ** Reactstrap Imports
import { Badge, UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap'
import Swal from 'sweetalert2'

// ** Vars
const states = ['success', 'danger', 'warning', 'info', 'dark', 'primary', 'secondary']

const status = {
    1: { title: 'Current', color: 'light-primary' },
    2: { title: 'Professional', color: 'light-success' },
    3: { title: 'Rejected', color: 'light-danger' },
    4: { title: 'Resigned', color: 'light-warning' },
    5: { title: 'Applied', color: 'light-info' }
}

export let data
// ** Get initial Data
const token = JSON.parse(localStorage.getItem("connectedId"))
console.log(token)
await fetch('https://api.mamifresh.ci/api/admin/filter/all', {
    method: "GET",
    headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
    },
}).then((res) => res.json()).then((result) => {
    console.log("OK")
    console.log(result.data)
    if (result.status == "success") {
        data = result.data
    } else {
        console.log("OK")
    }
}).catch((err) => console.log(err))



// ** Table Zero Config Column
export const basicColumns = [
    {
        name: 'ID',
        sortable: true,
        maxWidth: '100px',
        selector: data => data.id
    },
    {
        name: 'Name',
        sortable: true,
        minWidth: '225px',
        selector: data => data.full_name
    },
    {
        name: 'Email',
        sortable: true,
        minWidth: '310px',
        selector: data => data.email
    },
    {
        name: 'Position',
        sortable: true,
        minWidth: '250px',
        selector: data => data.post
    },
    {
        name: 'Age',
        sortable: true,
        minWidth: '100px',
        selector: data => data.age
    },
    {
        name: 'Salary',
        sortable: true,
        minWidth: '175px',
        selector: data => data.salary
    }
]
// ** Table ReOrder Column
export const reOrderColumns = [
    {
        name: 'ID',
        reorder: true,
        sortable: true,
        maxWidth: '100px',
        selector: data => data.id
    },
    {
        name: 'Name',
        reorder: true,
        sortable: true,
        minWidth: '225px',
        selector: data => data.full_name
    },
    {
        name: 'Email',
        reorder: true,
        sortable: true,
        minWidth: '310px',
        selector: data => data.email
    },
    {
        name: 'Position',
        reorder: true,
        sortable: true,
        minWidth: '250px',
        selector: data => data.post
    },
    {
        name: 'Age',
        reorder: true,
        sortable: true,
        minWidth: '100px',
        selector: data => data.age
    },
    {
        name: 'Salary',
        reorder: true,
        sortable: true,
        minWidth: '175px',
        selector: data => data.salary
    }
]

// ** Expandable table component
const ExpandableTable = ({ data }) => {
    return (
        <div className='expandable-content p-2'>
            <p>
                <span className='fw-bold'>City:</span> {data.city}
            </p>
            <p>
                <span className='fw-bold'>Experience:</span> {data.experience}
            </p>
            <p className='m-0'>
                <span className='fw-bold'>Post:</span> {data.post}
            </p>
        </div>
    )
}

// ** Table Common Column
export const columns = [
    {
        name: 'Icone',
        maxWidth: '100px',
        sortable: data => data.full_name,
        cell: data => (
            <div className='d-flex align-items-center'>
                {data.icon == [] ? (
                    <Avatar color={`light-${states[data.status]}`} content={data.full_name} initials />
                ) : (
                    <Avatar img={data.icon} />
                )}
            </div>
        )
    },
    {
        name: 'Nom',
        sortable: true,
        maxWidth: '250px',
        selector: data => data.name
    },
    {
        name: 'Visibilite',
        sortable: true,
        minWidth: '150px',
        selector: data => data.public
    }
]

// ** Table Intl Column
export const multiLingColumns = [
    {
        name: 'Name',
        sortable: true,
        minWidth: '200px',
        selector: data => data.full_name
    },
    {
        name: 'Position',
        sortable: true,
        minWidth: '250px',
        selector: data => data.post
    },
    {
        name: 'Email',
        sortable: true,
        minWidth: '250px',
        selector: data => data.email
    },
    {
        name: 'Date',
        sortable: true,
        minWidth: '150px',
        selector: data => data.start_date
    },

    {
        name: 'Salary',
        sortable: true,
        minWidth: '150px',
        selector: data => data.salary
    },
    {
        name: 'Status',
        sortable: true,
        minWidth: '150px',
        selector: data => data.status,
        cell: data => {
            return (
                <Badge color={status[data.status].color} pill>
                    {status[data.status].title}
                </Badge>
            )
        }
    },
    {
        name: 'Actions',
        allowOverflow: true,
        cell: () => {
            return (
                <div className='d-flex'>
                    <UncontrolledDropdown>
                        <DropdownToggle className='pe-1' tag='span'>
                            <MoreVertical size={15} />
                        </DropdownToggle>
                        <DropdownMenu end>
                            <DropdownItem>
                                <FileText size={15} />
                                <span className='align-middle ms-50'>Details</span>
                            </DropdownItem>
                            <DropdownItem>
                                <Archive size={15} />
                                <span className='align-middle ms-50'>Archive</span>
                            </DropdownItem>
                            <DropdownItem>
                                <Trash size={15} />
                                <span className='align-middle ms-50'>Delete</span>
                            </DropdownItem>
                        </DropdownMenu>
                    </UncontrolledDropdown>
                    <Edit size={15} />
                </div>
            )
        }
    }
]

// ** Table Server Side Column
export const serverSideColumns = [
    {
        sortable: true,
        name: 'Full Name',
        minWidth: '225px',
        selector: data => data.full_name
    },
    {
        sortable: true,
        name: 'Email',
        minWidth: '250px',
        selector: data => data.email
    },
    {
        sortable: true,
        name: 'Position',
        minWidth: '250px',
        selector: data => data.post
    },
    {
        sortable: true,
        name: 'Office',
        minWidth: '150px',
        selector: data => data.city
    },
    {
        sortable: true,
        name: 'Start Date',
        minWidth: '150px',
        selector: data => data.start_date
    },
    {
        sortable: true,
        name: 'Salary',
        minWidth: '150px',
        selector: data => data.salary
    }
]

// ** Table Adv Search Column
export const advSearchColumns = [
    {
        name: 'Name',
        sortable: true,
        minWidth: '200px',
        selector: data => data.full_name
    },
    {
        name: 'Email',
        sortable: true,
        minWidth: '250px',
        selector: data => data.email
    },
    {
        name: 'Post',
        sortable: true,
        minWidth: '250px',
        selector: data => data.post
    },
    {
        name: 'City',
        sortable: true,
        minWidth: '150px',
        selector: data => data.city
    },
    {
        name: 'Date',
        sortable: true,
        minWidth: '150px',
        selector: data => data.start_date
    },

    {
        name: 'Salary',
        sortable: true,
        minWidth: '100px',
        selector: data => data.salary
    }
]

export default ExpandableTable
