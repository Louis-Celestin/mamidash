import { Mail, Home } from "react-feather";

export default [
  {
    id: "home",
    title: "Dashboard",
    icon: <Home size={20} />,
    navLink: "/home"
  },
  {
    id: "products",
    title: "Produits",
    icon: <Mail size={20} />,
    children: [
      {
        id: "listProducts",
        title: "Liste des produits",
        icon: <Mail size={20} />,
        navLink: "/products/list",
      },
      {
        id: "listCategories",
        title: "Liste des categories",
        icon: <Mail size={20} />,
        navLink: "/products/categories/list",
      },
      {
        id: "listFilters",
        title: "Liste des filtres",
        icon: <Mail size={20} />,
        navLink: "/products/filters/list",
      }
    ]
  }
]